# Free Coin ERC20 implementation using Truffle, Ganache, mocha and chai

This project contains a implementation of ERC20 protocol.

## How to launch test

Clone the repository in local :
    
`git clone git@bitbucket.org:MatthieuMJ/freecoin-erc20.git`

Install node module :
    
`npm i`

Launch Ganache :

`ganache-cli`

Launch test :
    
`truffle test`