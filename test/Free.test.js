const BigNumber = web3.BigNumber;
const FreeToken = artifacts.require('Free');

require('chai')
    .use(require('chai-bignumber')(BigNumber))
    .should();

contract('Free', accounts => {
    const to = "0x8b1802733103ef415ed848f104c440d9426fc308"; // ganache-cli account[1]
    const from = "0x9d12e3bb73da9e4168b85ace44f4fee0eb840bd3"; // ganache-cli account[0]

    beforeEach(async function() {
        this.free = await FreeToken.new();
    })

    describe('Total Supply function check', function() {
        it('has the correct TotalSupply',async function() {
            const actualTotalSupply = (await this.free.totalSupply()).toString();
            const expectedTotalSupply = '250';
            actualTotalSupply.should.be.equal(expectedTotalSupply);
        });
    });

    describe('Balance of function check', function() {
        it('has the correct Balance for address from : 0x9d12e3bb73da9e4168b85ace44f4fee0eb840bd3',async function() {
            const actualBalance = (await this.free.balanceOf(from)).toString();
            const expectBalance = '250';
            actualBalance.should.be.equal(expectBalance);
        });
        it('has the correct Balance for address to : 0x8b1802733103ef415ed848f104c440d9426fc308',async function() {
            const actualBalance = (await this.free.balanceOf(to)).toString();
            const expectBalance = '0';
            actualBalance.should.be.equal(expectBalance);
        });
    });

    describe('Allowance function check', function() {
        it('has the correct amount allowed', async function() {
            const actualAllowedAmount = (await this.free.allowance(from, to)).toString();
            const expectedAllowedAmount = '0';
            actualAllowedAmount.should.be.equal(expectedAllowedAmount);
        })
    });

    describe('Transfer function check', function() {
        it('transfer 10 freecoin from 0x9d12e3bb73da9e4168b85ace44f4fee0eb840bd3 to 0x8b1802733103ef415ed848f104c440d9426fc308', async function() {
            await this.free.transfer(to, 10);
            const actualBalance = (await this.free.balanceOf(to)).toString();
            const expectedBalance = '10';
            actualBalance.should.be.equal(expectedBalance)
        });
    });

    describe('Approve function check', function() {
        it('approve 10 freecoin to 0x8b1802733103ef415ed848f104c440d9426fc308 from 0x9d12e3bb73da9e4168b85ace44f4fee0eb840bd3 account', async function() {
            await this.free.approve(to, 10);
            const actualAllowedAmount = (await this.free.allowance(from, to)).toString();
            const expectedAllowedAmount = '10';
            actualAllowedAmount.should.be.equal(expectedAllowedAmount);
        })
    });

    describe('TransferFrom function check', function() {
        it('approve / transfer 10 freecoin to 0x8b1802733103ef415ed848f104c440d9426fc308 from 0x9d12e3bb73da9e4168b85ace44f4fee0eb840bd3 account', async function() {
            await this.free.approve(from, 10);
            await this.free.transferFrom(from, 5);
            const actualBalance = (await this.free.balanceOf(from)).toString();
            const expectedBalance = '250';
            actualBalance.should.be.equal(expectedBalance);
        })
    });
})